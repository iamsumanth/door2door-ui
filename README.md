# README

1. Clone the repository

2. Install dependencies 
    * `npm install`

3. Run application
    * `npm run dev`

4. Run tests
    * `npm run test`

These steps should bring up your rails server.
