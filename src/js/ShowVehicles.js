import React, { Component } from 'react';
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps"
import { CITY_CENTER_COORDINATES, CAR } from '../constants/appConstants';

const getCarIcon = (rotation) => {
    return Object.assign({}, CAR, { rotation: rotation });
}

const ShowVehicles = ({ vehicles }) => {
    return (
        <GoogleMap
            defaultZoom={12}
            defaultCenter={ CITY_CENTER_COORDINATES }
        >
        {
            vehicles.map(vehicle => (
                <Marker
                    key={ vehicle.vehicle_id }
                    position={{ lat: vehicle.lat, lng: vehicle.lng }}
                    icon={ getCarIcon(vehicle.rotation) }
                />
            ))
        }
        </GoogleMap>
    );
};

export default ShowVehicles;