import React, { Component } from 'react';
import compose from 'recompose/compose';
import lifecycle from 'recompose/lifecycle';
import setDisplayName from 'recompose/setDisplayName';
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps"

import ShowVehicles from './ShowVehicles';
import { GOOGLE_API_KEY } from '../constants/appConstants';

const VehiclesMap = ({ vehicles }) => {
    return (
    <div> 
        <SetupMaps
          googleMapURL={ GOOGLE_API_KEY }
          loadingElement={<div style={{ height: `100%` }} />}
          containerElement={<div style={{ height: `400px` }} />}
          mapElement={<div style={{ height: `100%` }} />}
          vehicles={ vehicles }
        />
    </div>
    );
};

const vehiclesMapLifeCycleMethods = lifecycle({
    componentDidMount() {
        const vehiclesAction = this.props.vehiclesAction;
        vehiclesAction();
        setInterval(vehiclesAction, 10000);
    }
});

export default compose(
    setDisplayName('VehiclesMap'),
    vehiclesMapLifeCycleMethods
)(VehiclesMap);

const SetupMaps = withScriptjs(withGoogleMap((props) => {
    return (
        <ShowVehicles vehicles={ props.vehicles }/>
    );
}));