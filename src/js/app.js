import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import vehiclesReducer from '../reducers/vehiclesReducer';
import VehiclesMapContainer from '../containers/VehiclesMapContainer';

const reducer = combineReducers({ vehiclesReducer });
const store = createStore(reducer,
  applyMiddleware(
    thunkMiddleware
  )
);

render(
  <Provider store={store}>
    <VehiclesMapContainer />
  </Provider>,
  document.getElementById('app')
)