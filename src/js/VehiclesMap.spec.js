import React from 'react';
import { shallow } from 'enzyme';
import { useFakeTimers } from 'sinon';
import VehiclesMap from './VehiclesMap';
import { GOOGLE_API_KEY } from '../constants/appConstants';

describe('VehiclesMap', () => {
    const couponsActionMock = jest.fn();
    const vehicles = [];
    const shallowVehiclesMap = () => {
        return shallow(
            <VehiclesMap 
                vehiclesAction={ couponsActionMock }
                vehicles={ vehicles }
            />, { lifecycleExperimental: true }
        );
    };
    describe('#componentDidMount', () => {
        it('should call couponsAction and call every 10 seconds', () => {
            const clock = useFakeTimers();
            shallowVehiclesMap();
            clock.tick(10000);
            clock.tick(10000);
            expect(couponsActionMock).toHaveBeenCalledTimes(3);
        });
    });
    // it('should setup maps by passing all required parameters', () => {
    //     const vehiclesMap = shallowVehiclesMap();
    //     console.log(vehiclesMap.render().first().find('ShowVehicles').first());
    //     const setupMapsProps = vehiclesMap.render().first().find('ShowVehicles').first().props();

    //     expect(setupMapsProps.vehicles).toBe(vehicles);
    // });
})