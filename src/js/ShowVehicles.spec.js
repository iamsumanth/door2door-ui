import React from 'react';
import { shallow , mount} from 'enzyme';
import sinon from 'sinon';
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps"
import ShowVehicles from './ShowVehicles';
import { CITY_CENTER_COORDINATES, CAR } from '../constants/appConstants';

describe('ShowVehicles', () => {
    const shallowShowVehicles = (vehicles) => {
        return shallow(<ShowVehicles
            vehicles={ vehicles }
            googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyAn8wV135rftge8C0f11iJj5HBYU1f4lFI&v=3.exp&libraries=geometry,drawing,places"
            loadingElement={<div style={{ height: `100%` }} />}
            containerElement={<div style={{ height: `400px` }} />}
            mapElement={<div style={{ height: `100%` }} />}
                        />);
    };
    const vehicle1 = {'lat': 13.0, 'lng': 14.0, 'vehicle_id': 'one', 'rotation': 0 };
    const vehicle2 = {'lat': 23.0, 'lng': 24.0, 'vehicle_id': 'two', 'rotation': 100 };
    const vehicles = [vehicle1, vehicle2];

    it('should set city center in Google maps', () => {
        const googleMapProps = shallowShowVehicles(vehicles).find('GoogleMap').props();
        expect(googleMapProps.defaultCenter).toBe(CITY_CENTER_COORDINATES);
    });

    it('should set zoom level to 10 in Google maps', () => {
        const googleMapProps = shallowShowVehicles(vehicles).find('GoogleMap').props();
        expect(googleMapProps.defaultZoom).toBe(12);
    });

    it('should render Marker for all vehicles', () => {
        const goolgeMap = shallowShowVehicles(vehicles).find('GoogleMap');
        const vehicleMarkers = goolgeMap.find('Marker');
        const firstVehicleMarkerProps = vehicleMarkers.first().props();
        const secondVehicleMarkerProps = vehicleMarkers.last().props();
        const expectedFirstVehicleCoordinates = { lat: vehicle1.lat, lng: vehicle1.lng };
        const expectedSecondVehicleCoordinates = { lat: vehicle2.lat, lng: vehicle2.lng };

        expect(vehicleMarkers.length).toBe(2);
        expect(firstVehicleMarkerProps.position).toEqual(expectedFirstVehicleCoordinates);
        expect(secondVehicleMarkerProps.position).toEqual(expectedSecondVehicleCoordinates);
    });

    it('should render Marker for all vehicles and assign CAR Icon with rotation', () => {
        const goolgeMap = shallowShowVehicles(vehicles).find('GoogleMap');
        const vehicleMarkers = goolgeMap.find('Marker');
        const firstVehicleMarkerProps = vehicleMarkers.first().props();
        const secondVehicleMarkerProps = vehicleMarkers.last().props();

        expect(vehicleMarkers.length).toBe(2);
        expect(firstVehicleMarkerProps.icon).toEqual(CAR);
        expect(firstVehicleMarkerProps.icon.rotation).toEqual(vehicle1.rotation);
        expect(secondVehicleMarkerProps.icon.rotation).toEqual(vehicle2.rotation);
    });
});
