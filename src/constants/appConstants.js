export const CITY_CENTER_COORDINATES = { lat: 52.53, lng: 13.403 };
export const GOOGLE_API_KEY = "https://maps.googleapis.com/maps/api/js?key=AIzaSyAn8wV135rftge8C0f11iJj5HBYU1f4lFI&v=3.exp&libraries=geometry,drawing,places";
export const CAR = {
    path: 'M32 18l-4-8h-6v-4c0-1.1-0.9-2-2-2h-18c-1.1 0-2 0.9-2 2v16l2 2h2.536c-0.341 0.588-0.536 1.271-0.536 2 0 2.209 1.791 4 4 4s4-1.791 4-4c0-0.729-0.196-1.412-0.536-2h11.073c-0.341 0.588-0.537 1.271-0.537 2 0 2.209 1.791 4 4 4s4-1.791 4-4c0-0.729-0.196-1.412-0.537-2h2.537v-6zM22 18v-6h4.146l3 6h-7.146z',
    rotation: 0,
    fillColor: '#000',
    strokeColor: '#000',
    fillOpacity: 1
};