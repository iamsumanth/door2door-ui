import axios from 'axios';
import { GET_VEHICLES_SUCCESS } from '../constants/actionConstants';

export const vehiclesAction = () => (dispatch) => {

    const successHandler = (response) => {
        dispatch({
            type: GET_VEHICLES_SUCCESS,
            vehicles: response
        });
    }

    return axios.
        get('http://localhost:3000/vehicles/')
        .then(({data}) => { return data; })
        .then(successHandler);
};