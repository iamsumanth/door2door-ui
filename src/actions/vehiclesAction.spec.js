import axios from 'axios';
import { vehiclesAction } from './vehiclesAction'
import { GET_VEHICLES_SUCCESS } from '../constants/actionConstants';

describe('Vehicles end point', () => {
    const expectedVehicles = [{}, {}];
    const dispatchSpy = jest.fn();
    const successResult = {
        data: expectedVehicles
    }

    const mockAndGetVehicleWithSuccess = async () => {
        jest.spyOn(axios, 'get').mockImplementation(() => Promise.resolve(successResult));
        return vehiclesAction()(dispatchSpy);
    }
    it('should make call to vehicles end point', async() => {
        const vehicles = await mockAndGetVehicleWithSuccess();
        expect(axios.get).toHaveBeenCalledWith('http://localhost:3000/vehicles/');
    });

    it('should dispatch GET_VEHICLES_SUCCESS action', async() => {
        const action = {
            type: GET_VEHICLES_SUCCESS,
            vehicles: expectedVehicles
        }
        const vehicles = await mockAndGetVehicleWithSuccess();
        expect(dispatchSpy).toHaveBeenCalledWith(action);
    });
});