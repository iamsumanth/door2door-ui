import vehiclesReducer from './vehiclesReducer';
import { GET_VEHICLES_SUCCESS } from '../constants/actionConstants';
import Immutable from 'immutable';

describe('Vehicles Reducer', () => {
    const initialState = Immutable.Map({ vehicles: [] });
    const vehicle1 = {'lat': 13.0, 'lng': 14.0, 'at': '2017-12-02T11:02:00.000Z'};
    const vehicle2 = {'lat': 23.0, 'lng': 24.0, 'at': '2017-12-02T11:02:00.000Z'};
    const vehicles = [vehicle1, vehicle2];
    const successAction = {
        type: GET_VEHICLES_SUCCESS,
        vehicles: vehicles
    };
    it('should store vehicles in redux state on GET_VEHICLES_SUCCESS action', () => {
        const expectedState = Immutable.Map({ vehicles: vehicles });
        const updatedState = vehiclesReducer(initialState, successAction);
        expect(updatedState).toEqual(expectedState);
    });

    it('should not modify state if type is irrelevant to this reducer', () => {
        const expectedState = initialState;
        const updatedState = vehiclesReducer(initialState, {type: 'Irrelevant'});
        expect(updatedState).toBe(initialState);
    });

    it('should initialise default state to empty array', () => {
        const oldState = undefined;
        const initialAction = { type: '@@redux/INIT' };
        const expectedState = Immutable.Map({ vehicles: [] });
        const updatedState = vehiclesReducer(oldState, initialAction);
        expect(updatedState).toEqual(expectedState);
    });
});