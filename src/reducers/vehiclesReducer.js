import Immutable from 'immutable';
import { GET_VEHICLES_SUCCESS } from '../constants/actionConstants';

const initialState = Immutable.Map({ vehicles: [] });

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_VEHICLES_SUCCESS:
            return state.setIn(['vehicles'], action.vehicles);
        default:
            return state;
    }
};