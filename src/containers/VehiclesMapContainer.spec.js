import React from 'react';
import { Map } from 'immutable';
import configureStore from 'redux-mock-store';
import { shallow } from 'enzyme';
import VehiclesMapContainer from './VehiclesMapContainer';

describe('VehiclesMapContainer', () => {
    let dispatchMock;
    let store;
    const expectedVehicles = [];
    beforeEach(() => {
        dispatchMock = jest.fn();
        store = configureStore()({
            vehiclesReducer: Map({ vehicles: expectedVehicles })
        })
        store.dispatch = dispatchMock;
    });
    it('should read vehicles from store and pass it down to VehiclesMap', () => {
        const vehiclesMapContainer = shallow(<VehiclesMapContainer store={ store }/>);
        expect(vehiclesMapContainer.props().vehicles).toBe(expectedVehicles);
    });

    it('should pass down dispatch wrapped vehiclesAction to VehiclesMap', () => {
        const vehiclesMapContainer = shallow(<VehiclesMapContainer store={ store }/>);
        const vehiclesAction = vehiclesMapContainer.props().vehiclesAction;
        vehiclesAction();
        expect(dispatchMock).toHaveBeenCalled();
    });
});