import { connect } from 'react-redux';
import VehiclesMap from '../js/VehiclesMap';
import { vehiclesAction } from '../actions/vehiclesAction';

const mapStateToProps = (state) => {
    return {
        vehicles: state.vehiclesReducer.getIn(['vehicles'])
    }
}

const mapDispatchToProps = {
    vehiclesAction
};

export default connect(mapStateToProps, mapDispatchToProps)(VehiclesMap);